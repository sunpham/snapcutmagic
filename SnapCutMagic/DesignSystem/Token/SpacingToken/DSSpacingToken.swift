//
//  DSSpacingToken.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 18/08/2023.
//

import UIKit

extension CGFloat {

    /// Represents a spacing size of 2 units.
    static let spacingSize1: CGFloat = 2

    /// Represents a spacing size of 4 units.
    static let spacingSize2: CGFloat = 4

    /// Represents a spacing size of 8 units.
    static let spacingSize3: CGFloat = 8

    /// Represents a spacing size of 12 units.
    static let spacingSize4: CGFloat = 12

    /// Represents a spacing size of 16 units.
    static let spacingSize5: CGFloat = 16

    /// Represents a spacing size of 20 units.
    static let spacingSize6: CGFloat = 20

    /// Represents a spacing size of 24 units.
    static let spacingSize7: CGFloat = 24

    /// Represents a spacing size of 28 units.
    static let spacingSize8: CGFloat = 28

    /// Represents a spacing size of 32 units.
    static let spacingSize9: CGFloat = 32

    /// Represents a spacing size of 36 units.
    static let spacingSize10: CGFloat = 36

    /// Represents a spacing size of 40 units.
    static let spacingSize11: CGFloat = 40

    /// Represents a spacing size of 44 units.
    static let spacingSize12: CGFloat = 44

    /// Represents a spacing size of 48 units.
    static let spacingSize13: CGFloat = 48

    /// Represents a spacing size of 52 units.
    static let spacingSize14: CGFloat = 52

    /// Represents a spacing size of 56 units.
    static let spacingSize15: CGFloat = 56
}
