//
//  SCHomeViewController.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 18/08/2023.
//

import UIKit
import RxSwift
import RxCocoa

final class SCHomeViewController: UIViewController {

    // MARK: - UI

    private lazy var containerView = makeContainerView()
    private lazy var drawableImageContainerView = makeDrawableImageContainerView()
    private lazy var drawableImageView = makeDrawableImageView()
    private lazy var actionsContainerView = makeActionsContainerView()
    private lazy var rightActionsContainerView = makeRightActionsContainerView()
    private lazy var removeButton = makeRemoveButton()
    private lazy var leftActionsContainerView = makeLeftActionsContainerView()

    private lazy var undoButton = makeUndoButton()
    private lazy var redoButton = makeRedoButton()
    private lazy var toolSettingsButton  = makePencilSettingsButton()

    private lazy var miniMapPreview = makeMiniMapPreview()

    private lazy var bottomActionsContainerView = makeBottomActionsContainerView()
    private lazy var bottomActionsContentContainerView = makeBottomActionsContentContainerView()

    // MARK: - Dependencies

    private let disposeBag = DisposeBag()
    private let router: SCHomeRouterProtocol

    public init(router: SCHomeRouterProtocol = SCHomeRouter()) {
        self.router  = router
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
}

extension SCHomeViewController: SCPencilSettingsDelegate {

    func pencilSettingsDidUpdate(_ settings: SCPencilSettings) {
        drawableImageView.setShapeColor(settings.pencilColor)
    }
}

extension SCHomeViewController: DSDrawableImageViewDelegate {
    func drawableImageViewBeginTouch(atStartPoint point: CGPoint) {
        miniMapPreview.updateMiniMapPreview(originalPoint: point)
    }

    func drawableImageViewDidFinishDrawNewLine(atEndPoint point: CGPoint) {
        miniMapPreview.updateMiniMapPreview(originalPoint: point)
    }

    func drawableImageViewDidFinishDrawNewShape(atEndPoint point: CGPoint) {
        miniMapPreview.updateMiniMapPreview(originalPoint: point)
    }
}

// MARK: - Privates

extension SCHomeViewController {

    private func setupViews() {
        setupConstraints()
    }

    private func setupConstraints() {
        var constraints: [NSLayoutConstraint] = []

        constraints += constraintsContainerView()
        constraints += constraintsDrawableImageView()
        constraints += constraintsActionsContainerView()
        constraints += constraintsRemoveButton()
        constraints += constraintsBottomActionsContentContainerView()
        constraints += constraintsMiniMapPreview()

        NSLayoutConstraint.activate(constraints)
    }

    private func constraintsContainerView() -> [NSLayoutConstraint] {
        view.addSubview(containerView)

        containerView.addArrangedSubview(drawableImageContainerView)
        containerView.addArrangedSubview(bottomActionsContainerView)

        return [
            containerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ]
    }

    private func constraintsDrawableImageView() -> [NSLayoutConstraint] {
        drawableImageContainerView.addSubview(drawableImageView)
        return drawableImageView.edgesAnchor.constraint(
            equalTo: view.edgesAnchor
        )
    }

    private func constraintsRemoveButton() -> [NSLayoutConstraint] {
        drawableImageContainerView.addSubview(removeButton)
        return [
            removeButton.centerXAnchor.constraint(equalTo: drawableImageContainerView.centerXAnchor),
            removeButton.bottomAnchor.constraint(
                equalTo: drawableImageContainerView.bottomAnchor,
                constant: -.spacingSize4
            )
        ]
    }

    private func constraintsActionsContainerView() -> [NSLayoutConstraint] {
        drawableImageContainerView.addSubview(actionsContainerView)

        actionsContainerView.addArrangedSubview(leftActionsContainerView)
        actionsContainerView.addArrangedSubview(rightActionsContainerView)

        leftActionsContainerView.addArrangedSubview(undoButton)
        leftActionsContainerView.addArrangedSubview(redoButton)

        rightActionsContainerView.addArrangedSubview(toolSettingsButton)


        return [
            actionsContainerView.leadingAnchor.constraint(
                equalTo: drawableImageContainerView.leadingAnchor,
                constant: .spacingSize4
            ),
            actionsContainerView.trailingAnchor.constraint(
                equalTo: drawableImageContainerView.trailingAnchor,
                constant: -.spacingSize4
            ),
            actionsContainerView.centerYAnchor.constraint(
                equalTo: removeButton.centerYAnchor
            )
        ]
    }


    private func constraintsBottomActionsContentContainerView() -> [NSLayoutConstraint] {
        bottomActionsContainerView.addSubview(bottomActionsContentContainerView)
        var constraints = bottomActionsContentContainerView.edgesAnchor.constraint(
            equalTo: bottomActionsContainerView.edgesAnchor,
            insets: UIEdgeInsets(
                top: .spacingSize4,
                left: .spacingSize4,
                bottom: -.spacingSize10,
                right: -.spacingSize4
            )
        )
        constraints.append(
            bottomActionsContentContainerView.heightAnchor.constraint(equalToConstant: .spacingSize15)
        )

        return constraints
    }

    private func constraintsMiniMapPreview() -> [NSLayoutConstraint] {
        view.addSubview(miniMapPreview)
        return [
            miniMapPreview.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: .spacingSize8),
            miniMapPreview.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .spacingSize4),
            miniMapPreview.widthAnchor.constraint(equalTo: drawableImageView.widthAnchor, multiplier: 0.4),
            miniMapPreview.heightAnchor.constraint(equalTo: miniMapPreview.widthAnchor, multiplier: 1.25)
        ]
    }

    private func setupNavigationBar() {
        let rightBarButton = UIBarButtonItem(
            title: "Save",
            style: .plain,
            target: self, action: #selector(onSave)
        )
        navigationItem.rightBarButtonItem = rightBarButton
    }

    private func makeContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = .zero
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makeDrawableImageContainerView() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makeDrawableImageView() -> DSDrawableImageView {
        let view = DSDrawableImageView()
        view.delegate = self
        view.setDrawableImage(.local(image: Asset.imFilteredPlaceholder.image))
        view.setShapeColor(SCPreferenceStorageService.pencilSettingsConfiguration.pencilColor)
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makeActionsContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .horizontal
        view.alignment = .center
        view.distribution = .equalCentering
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makeRightActionsContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .fill
        view.alignment = .center
        view.spacing = .spacingSize2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makeLeftActionsContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .fill
        view.alignment = .center
        view.spacing = .spacingSize4
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makeUndoButton() -> DSButton {
        let button = DSButton(style: .secondaryOnDark, size: .medium, asset: .local(image: Asset.icUndoSmall.image))
        button.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.handleUndoAction()
            })
            .disposed(by: disposeBag)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }

    private func makeRedoButton() -> DSButton {
        let button = DSButton(style: .secondaryOnDark, size: .medium, asset: .local(image: Asset.icRedoSmall.image))
        button.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.handleRedoAction()
            })
            .disposed(by: disposeBag)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }

    private func makePencilSettingsButton() -> DSButton {
        let button = DSButton(style: .secondaryOnDark, size: .medium, asset: .local(image: Asset.icSettingsSmall.image))
        button.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.showFilterTool()
            })
            .disposed(by: disposeBag)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }

    private func makeBottomActionsContainerView() -> UIView {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makeBottomActionsContentContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .fillEqually
        view.alignment = .fill
        view.spacing = .spacingSize4
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makeRemoveButton() -> DSButton {
        let button = DSButton(
            style: .primary,
            size: .medium,
            title: "Remove",
            asset: .local(image: Asset.icCancelLarge.image)
        )
        button.rx.tap
            .subscribe(onNext: { [weak self] in
                self?.miniMapPreview.reset()
                self?.drawableImageView.reset()
            })
            .disposed(by: disposeBag)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }

    private func makeMiniMapPreview() -> DSMiniMapView {
        let miniMapView = DSMiniMapView(mainView: drawableImageView)
        miniMapView.layer.cornerRadius = .spacingSize4
        miniMapView.translatesAutoresizingMaskIntoConstraints = false
        return miniMapView
    }

    private func handleUndoAction() {
        drawableImageView.undoPreviousState()
    }

    private func handleRedoAction() {
        drawableImageView.redoPreviousState()
    }

    private func showFilterTool() {
        router.showPencilSettings(from: self, delegate: self)
    }

    @objc private func onSave() {
        guard let image = drawableImageView.export() else { return }
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }

    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        router.showSavedImageAlert(from: self, error: error)
    }
}
