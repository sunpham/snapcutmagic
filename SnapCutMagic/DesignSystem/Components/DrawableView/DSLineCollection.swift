//
//  DSLineCollection.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 19/08/2023.
//

import Foundation

struct DSLineCollection: Sequence {

    // MARK: - Properties

    private var lines: [DSLine] = [DSLine]()

    // MARK: - Sequence Conformance

    func makeIterator() -> IndexingIterator<[DSLine]> {
        return lines.makeIterator()
    }

    func last() -> DSLine? {
        return lines.last
    }

    // MARK: - Methods

    /// Creates a new line collection by adding a line to the existing collection.
    ///
    /// - Parameter line: The line to be added to the collection.
    /// - Returns: A new `LineCollection` instance with the added line.
    func lineCollectionByAddingLine(line: DSLine) -> DSLineCollection {
        var lineCollection = self
        lineCollection.lines.append(line)
        return lineCollection
    }

    /// Creates a new line collection by adding a point to the last line in the collection.
    /// If the collection is empty, a new line is created and the point is added to it.
    ///
    /// - Parameter point: The point to be added to the last line in the collection.
    /// - Returns: A new `LineCollection` instance with the added point.
    func lineCollectionByAddingPoint(point: CGPoint) -> DSLineCollection {
        var lineCollection = self

        if lineCollection.lines.isEmpty {
            lineCollection.lines.append(DSLine())
        }

        var lastLine = lineCollection.lines.last!
        lastLine = lastLine.lineByAdding(point: point)
        let lineCount = lineCollection.lines.count
        lineCollection.lines.replaceSubrange(lineCount - 1 ..< lineCount, with: [lastLine])

        return lineCollection
    }
}
