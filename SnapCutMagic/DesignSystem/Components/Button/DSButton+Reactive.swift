//
//  DSButton+Reactive.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 19/08/2023.
//

import RxSwift
import RxCocoa

extension Reactive where Base: DSButton {

    /// Reactive wrapper for the button's tap event.
    public var tap: ControlEvent<Void> {
        return ControlEvent(events: base.didTapRelay)
    }
}
