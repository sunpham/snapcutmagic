//
//  SCUserDefaults.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 21/08/2023.
//

import Foundation

@propertyWrapper
struct SCUserDefaults<Value> {

    let key: String
    let defaultValue: Value
    var container: UserDefaults = .standard

    var wrappedValue: Value {
        get {
            return container.object(forKey: key) as? Value ?? defaultValue
        }
        set {
            container.set(newValue, forKey: key)
        }
    }
}
