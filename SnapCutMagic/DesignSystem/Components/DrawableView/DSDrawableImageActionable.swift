//
//  DSDrawableImageActionable.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 19/08/2023.
//

import UIKit

/// A protocol defining actions related to undoing and redoing drawing states.
public protocol DSDrawableImageActionable: AnyObject {

    /// Undo the previous drawing state.
    func undoPreviousState()

    /// Redo the previously undone drawing state.
    func redoPreviousState()

    /// Reset the drawing to its initial state.
    func reset()

    /// Export the current drawing as an image or other appropriate format.
    func export() -> UIImage?
}
