//
//  DSDrawableImageView.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 18/08/2023.
//

import UIKit

public final class DSDrawableImageView: UIView {

    // MARK: - UI

    private lazy var foregroundImageView = makeForegroundImageView()

    // MARK: - Configuration Properties

    private(set) var lineWidth: CGFloat = 2
    private(set) var drawingColor: UIColor = .red

    // MARK: - Data

    private(set) var previousLocation: CGPoint?
    private(set) var lineCollection: DSLineCollection = DSLineCollection()

    private(set) var shapeLayers: [CAShapeLayer] = []
    var currentPath: CGPath?

    // MARK: - Dependencies

    weak var delegate: DSDrawableImageViewDelegate?

    var drawingLine: CAShapeLayer? = nil

    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        backgroundColor = .clear
    }

    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }

        let location = touch.location(in: self)
        delegate?.drawableImageViewBeginTouch(atStartPoint: location)
    }

    public override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }

        let location = touch.location(in: self)
        if location == previousLocation {
            return
        }

        if previousLocation != nil {
            lineCollection = lineCollection.lineCollectionByAddingPoint(point: location)
        } else {
            let line = DSLine().lineByAdding(point: location)
            lineCollection = lineCollection.lineCollectionByAddingLine(line: line)
        }
        drawLastLine(in: lineCollection)
        previousLocation = location
        
        if let endPoint = previousLocation {
            delegate?.drawableImageViewDidFinishDrawNewShape(atEndPoint: endPoint)
        }
    }

    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let currentLine = lineCollection.last() else { return }
        let shapeLayer = makeShape(for: currentLine)
        shapeLayer.name = generatedUniqueShapeName()

        addNewShapeLayer(shapeLayer)
        addNewShapeLayerUndoRegister(shapeLayer)

        if let endPoint = previousLocation {
            delegate?.drawableImageViewDidFinishDrawNewShape(atEndPoint: endPoint)
        }

        previousLocation = nil

        drawingLine?.removeFromSuperlayer()
        drawingLine = nil
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        foregroundImageView.frame = bounds
    }

    public override func draw(_ rect: CGRect) {
        super.draw(rect)
    }

    public override func draw(_ layer: CALayer, in context: CGContext) {
        super.draw(layer, in: context)
        foregroundImageView.layer.render(in: context)
    }
}

// MARK: - DSDrawableImageConfigurable

extension DSDrawableImageView: DSDrawableImageConfigurable {

    public func setLineWidth(_ width: CGFloat) {
        lineWidth = width
    }

    public func setShapeColor(_ color: UIColor) {
        drawingColor = color
    }

    public func setDrawableImage(_ asset: DSAssetType) {
        switch asset {
        case .local(let image):
            foregroundImageView.image = image

        case .remote(_):
            // will be upgrade later
            break
        }
    }
}

// MARK: - DSDrawableImageActionable

extension DSDrawableImageView: DSDrawableImageActionable {

    public func undoPreviousState() {

        guard let canUndo = undoManager?.canUndo, canUndo else { return }
        self.undoManager?.undo()
    }

    public func redoPreviousState() {
        guard let canRedo = undoManager?.canRedo, canRedo else { return }
        self.undoManager?.redo()
    }

    public func reset() {
        for shapeLayer in shapeLayers {
            shapeLayer.removeFromSuperlayer()
        }
        shapeLayers.removeAll()
    }

    public func export() -> UIImage? {

        guard let image = foregroundImageView.image else { return nil }
        let originalImageSize = image.size
        let imageViewSize = frame.size
        let scale = originalImageSize.height / imageViewSize.height

        return exportAsImage(scale: scale)
    }
}

// MARK: - Privates

extension DSDrawableImageView {

    private func makeForegroundImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }

    private func generatedUniqueShapeName() -> String {
        let suffix = UUID().uuidString
        let prefix = "ds_drawable_shape_layer"
        return [prefix, suffix].joined(separator: "_")
    }

    private func addNewShapeLayerUndoRegister(_ shapeLayer: CAShapeLayer) {

        guard let undoManager else { return }
        undoManager.registerUndo(withTarget: self, handler: { target in
            target.removeShapeLayer(shapeLayer)
            target.removeShapeLayerUndoRegister(shapeLayer)
        })
    }

    private func addNewShapeLayer(_ shapeLayer: CAShapeLayer) {
        shapeLayers.append(shapeLayer)
        layer.addSublayer(shapeLayer)
    }

    private func removeShapeLayerUndoRegister(_ shapeLayer: CAShapeLayer) {
        guard let undoManager else { return }
        undoManager.registerUndo(withTarget: self, handler: { target in
            target.addNewShapeLayer(shapeLayer)
            target.addNewShapeLayerUndoRegister(shapeLayer)
        })
    }

    private func removeShapeLayer(_ shapeLayer: CAShapeLayer) {
        guard let removedShapeIndex = shapeLayers.firstIndex(where: { $0.name == shapeLayer.name })
        else { return }
        shapeLayers.remove(at: removedShapeIndex)
        shapeLayer.removeFromSuperlayer()
    }

    private func makeBezierPath(from line: DSLine) -> UIBezierPath {
        let path = UIBezierPath()

        for (index, point) in line.enumerated() {
            if index == 0 {
                path.move(to: point)
            } else {
                path.addLine(to: point)
            }
        }

        return path
    }

    private func makeShape(for line: DSLine) -> CAShapeLayer {
        let path = makeBezierPath(from: line)
        if let firstPoint = line.first {
            path.addLine(to: firstPoint)
        }

        let shapeLayer = CAShapeLayer()
        shapeLayer.lineCap = .round
        shapeLayer.lineJoin = .round
        shapeLayer.fillMode = .backwards
        shapeLayer.fillRule = .evenOdd
        shapeLayer.strokeColor = drawingColor.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.path = path.cgPath
        shapeLayer.fillColor = drawingColor.withAlphaComponent(0.5).cgColor
       return shapeLayer
    }

    private func drawLastLine(in lineCollection: DSLineCollection) {

        guard let line = lineCollection.last() else { return }
        let path = makeBezierPath(from: line)

        if let drawingLine {
            drawingLine.path = path.cgPath
        } else {
            let shapeLayer = CAShapeLayer()
            shapeLayer.lineCap = .round
            shapeLayer.lineJoin = .round
            shapeLayer.strokeColor = drawingColor.cgColor
            shapeLayer.path = path.cgPath
            shapeLayer.fillColor = UIColor.clear.cgColor
            layer.addSublayer(shapeLayer)
            drawingLine = shapeLayer
        }
    }
}
