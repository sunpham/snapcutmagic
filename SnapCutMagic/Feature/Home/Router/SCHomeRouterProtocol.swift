//
//  SCHomeRouterProtocol.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 19/08/2023.
//

import UIKit

public protocol SCHomeRouterProtocol: AnyObject {

    /// Presents the pencil settings view.
    ///
    /// - Parameters:
    ///   - parent: The parent view controller from which to present the settings view.
    ///   - delegate: The delegate for pencil settings actions.
    func showPencilSettings(from parent: UIViewController, delegate: SCPencilSettingsDelegate?)

    /// Presents an alert indicating that the image was saved or an error occurred while saving.
    ///
    /// - Parameters:
    ///   - parent: The parent view controller from which to present the alert.
    ///   - error: An optional error that occurred during the saving process, if any.
    func showSavedImageAlert(from parent: UIViewController, error: Error?)
}
