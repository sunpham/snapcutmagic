//
//  DSButtonStyle.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 19/08/2023.
//

import UIKit

public enum DSButtonStyle {

    /// Primary button style with a solid color background.
    case primary

    /// Secondary button style suitable for dark backgrounds.
    case secondaryOnDark

    /// Background color corresponding to the button style.
    var backgroundColor: UIColor {

        switch self {
        case .primary:
            return .blue

        case .secondaryOnDark:
            // Using black with reduced opacity for a semi-transparent effect
            return .black.withAlphaComponent(0.5)
        }
    }

    /// Title text color based on the button style.
    var titleColor: UIColor {
        switch self {
        case .primary:
            return .white

        case .secondaryOnDark:
            return .white
        }
    }
}
