//
//  UIView+Extensions.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 18/08/2023.
//

import UIKit

public extension UIView {
    struct EdgesAnchor {
        var topAnchor: NSLayoutYAxisAnchor
        var leadingAnchor: NSLayoutXAxisAnchor
        var bottomAnchor: NSLayoutYAxisAnchor
        var trailingAnchor: NSLayoutXAxisAnchor
    }

    var edgesAnchor: UIView.EdgesAnchor {
        return UIView.EdgesAnchor(topAnchor: topAnchor,
                                  leadingAnchor: leadingAnchor,
                                  bottomAnchor: bottomAnchor,
                                  trailingAnchor: trailingAnchor)
    }
}

public extension UIView.EdgesAnchor {
    func constraint(equalTo edges: UIView.EdgesAnchor, insets: UIEdgeInsets = .zero) -> [NSLayoutConstraint] {
        return [
            topAnchor.constraint(equalTo: edges.topAnchor, constant: insets.top),
            leadingAnchor.constraint(equalTo: edges.leadingAnchor, constant: insets.left),
            trailingAnchor.constraint(equalTo: edges.trailingAnchor, constant: insets.right),
            bottomAnchor.constraint(equalTo: edges.bottomAnchor, constant: insets.bottom)
        ]
    }
}

extension UIView {

    func exportAsImage(scale: CGFloat) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(frame.size, true, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let cgImage = image?.cgImage {
            return UIImage(cgImage: cgImage)
        }
        return nil
    }
}
