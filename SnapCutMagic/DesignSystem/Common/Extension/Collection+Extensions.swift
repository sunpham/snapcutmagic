//
//  Collection+Extensions.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 19/08/2023.
//

import Foundation

extension Collection {

    /// Returns the element at the specified index if it exists, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
