//
//  DSButton.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 19/08/2023.
//

import UIKit
import RxCocoa
import RxSwift

public final class DSButton: UIControl {

    // MARK: - UI

    private lazy var containerView = makeContainerView()
    private lazy var contentContainerView = makeContentContainerView()
    private lazy var iconImageView = makeIconImageView()
    private lazy var titleLabel = makeTitleLabel()

    // MARK: - Constraints

    private lazy var containerHeightConstraint = makeContainerHeightConstraint()
    private lazy var contentLeadingConstraint = makeContentLeadingConstraint()
    private lazy var contentTrailingConstraint = makeContentTrailingConstraint()

    // MARK: - Properties

    private var style: DSButtonStyle
    private var size: DSButtonSize
    private var title: String?
    private var asset: DSAssetType?

    // MARK: - Data:

    let didTapRelay = PublishRelay<Void>()

    // MARK: - Initializers

    public init(
        style: DSButtonStyle,
        size: DSButtonSize = .medium,
        title: String? = nil,
        asset: DSAssetType? = nil
    ) {
        self.style = style
        self.size = size
        self.title = title
        self.asset = asset
        super.init(frame: .zero)
        setupViews()
        bindingData()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func setStyle(_ style: DSButtonStyle) {
        self.style = style
        containerView.backgroundColor = style.backgroundColor
        titleLabel.textColor = style.titleColor
    }

    public func setTitle(_ title: String?) {
        self.title = title

        if let title, !title.isEmpty {
            contentLeadingConstraint.constant = .spacingSize5
            contentTrailingConstraint.constant = -.spacingSize5
            titleLabel.isHidden = false
            titleLabel.text = title
        } else {
            contentLeadingConstraint.constant = .spacingSize3
            contentTrailingConstraint.constant = -.spacingSize3
            titleLabel.isHidden = true
        }
    }

    public func setSize(_ size: DSButtonSize) {
        self.size = size
        containerHeightConstraint.constant = size.height
        containerView.layer.cornerRadius = size.height / 2
    }

    public func setImage(_ asset: DSAssetType?) {
        self.asset = asset
        guard let asset else {
            iconImageView.isHidden = true
            return
        }

        switch asset {
        case .local(let image):
            iconImageView.image = image

        case .remote(_):
            break
        }
    }
}

// MARK: - Privates

extension DSButton {

    private func setupViews() {
        setupConstraints()
    }

    private func bindingData() {
        setStyle(style)
        setSize(size)
        setTitle(title)
        setImage(asset)
    }

    private func setupConstraints() {
        var constraints: [NSLayoutConstraint] = []

        constraints += constraintsContainerView()
        constraints += constraintsContentContainerView()
        constraints += constraintsIconImageView()

        NSLayoutConstraint.activate(constraints)
    }

    private func constraintsContainerView() -> [NSLayoutConstraint] {
        addSubview(containerView)
        var constraints = containerView.edgesAnchor.constraint(equalTo: edgesAnchor)
        constraints.append(containerHeightConstraint)
        return constraints
    }

    private func constraintsContentContainerView() -> [NSLayoutConstraint] {
        containerView.addSubview(contentContainerView)

        contentContainerView.addArrangedSubview(iconImageView)
        contentContainerView.addArrangedSubview(titleLabel)

        return [
            contentContainerView.topAnchor.constraint(
                equalTo: containerView.topAnchor,
                constant: .spacingSize3
            ),
            contentLeadingConstraint,
            contentTrailingConstraint,
            contentContainerView.bottomAnchor.constraint(
                equalTo: containerView.bottomAnchor,
                constant: -.spacingSize3
            )
        ]
    }

    private func constraintsIconImageView() -> [NSLayoutConstraint] {
        return [
            iconImageView.widthAnchor.constraint(equalTo: iconImageView.heightAnchor)
        ]
    }

    private func makeContainerView() -> UIView {
        let view = UIView()
        let tapGesture = UITapGestureRecognizer()
        tapGesture.numberOfTapsRequired = 1
        tapGesture.addTarget(self, action: #selector(onTap))
        view.addGestureRecognizer(tapGesture)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makeContentContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .fill
        view.alignment = .fill
        view.spacing = .spacingSize2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makeIconImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }

    private func makeTitleLabel() -> UILabel {
        let label = UILabel()
        label.font = .paragraph1Bold
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.numberOfLines = .zero
        return label
    }

    private func makeContainerHeightConstraint() ->  NSLayoutConstraint {
        return containerView.heightAnchor.constraint(
            equalToConstant: DSButtonSize.medium.height
        )
    }

    private func makeContentLeadingConstraint() ->  NSLayoutConstraint {
        return contentContainerView.leadingAnchor.constraint(
            equalTo: containerView.leadingAnchor,
            constant: .spacingSize5
        )
    }

    private func makeContentTrailingConstraint() ->  NSLayoutConstraint {
        return contentContainerView.trailingAnchor.constraint(
            equalTo: containerView.trailingAnchor,
            constant: -.spacingSize5
        )
    }

    @objc private func onTap() {
        didTapRelay.accept(())
    }
}
