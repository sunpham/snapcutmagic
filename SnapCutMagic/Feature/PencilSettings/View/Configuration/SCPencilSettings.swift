//
//  SCPencilSettings.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 21/08/2023.
//

import UIKit

public final class SCPencilSettings {

    // MARK: - Properties

    public var pencilColor: UIColor

    // MARK: - Initializers

    public init(pencilColor: UIColor) {
        self.pencilColor = pencilColor
    }
}

// MARK: - Equatable

extension SCPencilSettings: Equatable {

    public static func == (lhs: SCPencilSettings, rhs: SCPencilSettings) -> Bool {
        return lhs.pencilColor === rhs.pencilColor
    }
}
