// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return prefer_self_in_static_references

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  internal enum Common {
    internal enum Action {
      /// Localizable.strings
      ///   SnapCutMagic
      /// 
      ///   Created by Tuan Pham on 21/08/2023.
      internal static let `continue` = L10n.tr("Localizable", "common.action.continue", fallback: "Continue")
    }
  }
  internal enum Home {
    internal enum SavingImage {
      internal enum Status {
        internal enum Error {
          /// Saved Image RanInto An Error
          internal static let title = L10n.tr("Localizable", "home.savingImage.status.error.title", fallback: "Saved Image RanInto An Error")
        }
        internal enum Success {
          /// Your altered image has been saved to your photos.
          internal static let message = L10n.tr("Localizable", "home.savingImage.status.success.message", fallback: "Your altered image has been saved to your photos.")
          /// Saved Image Successfully
          internal static let title = L10n.tr("Localizable", "home.savingImage.status.success.title", fallback: "Saved Image Successfully")
        }
      }
    }
  }
  internal enum PencilSettings {
    internal enum PickPencilColor {
      /// Pencil Color
      internal static let title = L10n.tr("Localizable", "pencilSettings.pickPencilColor.title", fallback: "Pencil Color")
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg..., fallback value: String) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: value, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
