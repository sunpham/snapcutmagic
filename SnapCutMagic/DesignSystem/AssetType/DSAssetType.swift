//
//  DSAssetType.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 19/08/2023.
//

import UIKit

/// An enumeration representing different types of assets.
public enum DSAssetType {

    /// Represents a local image asset.
    /// - Parameter image: The UIImage representing the local image.
    case local(image: UIImage)

    /// Represents a remote image asset.
    /// - Parameter imageURLString: The URL string of the remote image.
    case remote(imageURLString: String)
}
