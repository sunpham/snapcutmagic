//
//  DSLine.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 19/08/2023.
//

import Foundation

struct DSLine: Sequence {

    // MARK: - Properties

    private var points: [CGPoint] = [CGPoint]()

    // MARK: - Sequence Conformance

    func makeIterator() -> IndexingIterator<[CGPoint]> {
        return points.makeIterator()
    }

    var first: CGPoint? {
        return points.first
    }

    // MARK: - Methods

    /// Creates a new line by adding a point to the existing line.
    ///
    /// - Parameter point: The point to be added to the line.
    /// - Returns: A new `Line` instance with the added point.
    func lineByAdding(point: CGPoint) -> DSLine {
        var line = self
        line.points.append(point)
        return line
    }
}
