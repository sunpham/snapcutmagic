//
//  SCPreferenceStorageService.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 21/08/2023.
//

import UIKit

struct SCPreferenceStorageService {

    @SCUserDefaults(
        key: "snapcut_magic_pencil_settings_configuration",
        defaultValue: SCPencilSettings(pencilColor: UIColor.yellow)
    )
    static var pencilSettingsConfiguration: SCPencilSettings
}
