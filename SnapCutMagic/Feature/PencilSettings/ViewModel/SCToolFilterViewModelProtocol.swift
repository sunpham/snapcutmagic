//
//  SCPencilSettingsViewModelProtocol.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 20/08/2023.
//

import Foundation
import RxSwift

public protocol SCPencilSettingsViewModelProtocol: AnyObject {

    var pencilSettings: Observable<SCPencilSettings> { get }

    func updatePencilColor(_ color: UIColor)
}
