//
//  DSZoomedImageView.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 25/08/2023.
//

import UIKit

public final class DSZoomedImageView: UIScrollView {

    // MARK: - UI

    lazy var imageView = makeImageView()

    var image: UIImage? {
        get {
            return imageView.image
        }

        set {
            imageView.image = newValue
        }
    }

    // MARK: - Initializers

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - UIScrollViewDelegate

extension DSZoomedImageView: UIScrollViewDelegate {
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {

        if scrollView.contentOffset.y < .zero {
            scrollView.contentOffset.y = .zero
        }

        if scrollView.contentOffset.x < .zero {
            scrollView.contentOffset.x = .zero
        }
    }
}

// MARK: - Privates

extension DSZoomedImageView {

    private func makeImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }

    private func setupViews() {
        backgroundColor = .clear
        setupConstraints()
        setupScrollView()
    }

    private func setupScrollView() {

        minimumZoomScale = 1
        maximumZoomScale = 50
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
        delegate = self
    }

    private func setupConstraints() {
        var constraints: [NSLayoutConstraint] = []

        constraints += constraintsMiniMapImageView()

        NSLayoutConstraint.activate(constraints)
    }

    private func constraintsMiniMapImageView() -> [NSLayoutConstraint] {
        addSubview(imageView)
        var constraints = imageView.edgesAnchor.constraint(equalTo: edgesAnchor)
        constraints += [
            imageView.heightAnchor.constraint(equalTo: heightAnchor),
            imageView.widthAnchor.constraint(equalTo: widthAnchor)
        ]

        return constraints
    }
}
