//
//  SCPencilSettingsDelegate.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 21/08/2023.
//

import Foundation

/// Protocol defining the interface for notifying changes in pencil settings.
public protocol SCPencilSettingsDelegate: AnyObject {

    /// Function called when pencil settings are updated.
    ///
    /// - Parameter settings: The updated pencil settings.
    func pencilSettingsDidUpdate(_ settings: SCPencilSettings)
}
