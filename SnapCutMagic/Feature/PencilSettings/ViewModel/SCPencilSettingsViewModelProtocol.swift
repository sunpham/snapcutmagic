//
//  SCPencilSettingsViewModelProtocol.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 20/08/2023.
//

import Foundation
import RxSwift

/// Protocol defining the interface for managing pencil settings.
public protocol SCPencilSettingsViewModelProtocol: AnyObject {

    /// An observable property that holds the current pencil settings.
    var pencilSettings: Observable<SCPencilSettings> { get }

    /// Function to update the pencil color in the settings.
    ///
    /// - Parameter color: The new color to be set for the pencil.
    func updatePencilColor(_ color: UIColor)

    /// Function to fetch and update the pencil settings.
    ///
    /// This function can be used to retrieve the latest pencil settings from a data source.
    func fetch()
}
