//
//  DSMiniMapView.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 23/08/2023.
//

import UIKit

public final class DSMiniMapView: UIView {

    // MARK: - UI

    private lazy var miniMapImageView = makeMiniMapImageView()

    // MARK: - Dependencies
    
    private weak var mainView: UIView?

    // MARK: - Data

    private var neverSetScaleRatioStandardBefore = true

    // MARK: - Initializers

    public init(mainView: UIView) {
        self.mainView = mainView
        super.init(frame: .zero)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func updateMiniMapPreview(originalPoint point: CGPoint) {
        guard let mainView = mainView else { return }
        miniMapImageView.setZoomScale(UIConfig.miniMapImageScale, animated: false)

        let imageScale = mainView.frame.height / frame.height
        traitCollection.performAsCurrent {
            if let miniMapImage = mainView.exportAsImage(scale: imageScale) {
                miniMapImageView.image = miniMapImage
            }

            let convertedPoint = calculatedDestinationScrollPoint(fromOriginalPoint: point)
            focusOn(point: convertedPoint)
        }
    }

    public func reset() {
        miniMapImageView.image = nil
    }
}

// MARK: - Privates

extension DSMiniMapView {

    private func setupViews() {
        backgroundColor = .clear
        setupConstraints()
    }

    private func setupConstraints() {
        var constraints: [NSLayoutConstraint] = []

        constraints += constraintsMiniMapImageView()

        NSLayoutConstraint.activate(constraints)
    }

    private func constraintsMiniMapImageView() -> [NSLayoutConstraint] {
        addSubview(miniMapImageView)
        return miniMapImageView.edgesAnchor.constraint(equalTo: edgesAnchor)
    }

    private func makeMiniMapImageView() -> DSZoomedImageView {
        let imageView = DSZoomedImageView(frame: .zero)
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }

    private func calculatedDestinationScrollPoint(
        fromOriginalPoint point: CGPoint
    ) -> CGPoint {
        guard let mainView = mainView else { return point }
        let mainViewHeight = mainView.frame.size.height
        let miniMapImageViewContentHeight = miniMapImageView.contentSize.height
        let ratio = miniMapImageViewContentHeight / mainViewHeight

        let convertedX = point.x * ratio + bounds.width / 2 + 32
        let convertedY = point.y * ratio - bounds.height / 2

        return CGPoint(x: convertedX, y: convertedY)
    }

    private func focusOn(point: CGPoint) {
        miniMapImageView.setContentOffset(point, animated: false)
    }
}

// MARK: - UIConfig

extension DSMiniMapView {

    enum UIConfig {
        static let miniMapImageScale: CGFloat = 6
    }
}
