//
//  SCPencilSettingsViewModel.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 21/08/2023.
//

import UIKit
import RxSwift
import RxRelay

final class SCPencilSettingsViewModel {

    private let pencilSettingsRelay = BehaviorRelay<SCPencilSettings?>(value: nil)

    init() {}
}

extension SCPencilSettingsViewModel: SCPencilSettingsViewModelProtocol {

    var pencilSettings: Observable<SCPencilSettings> {
        return pencilSettingsRelay.compactMap { $0 }
    }

    func updatePencilColor(_ color: UIColor) {
        guard let modifyingSettings = pencilSettingsRelay.value else { return }
        modifyingSettings.pencilColor = color
        pencilSettingsRelay.accept(modifyingSettings)
    }

    func fetch() {
        pencilSettingsRelay.accept(SCPreferenceStorageService.pencilSettingsConfiguration)
    }
}
