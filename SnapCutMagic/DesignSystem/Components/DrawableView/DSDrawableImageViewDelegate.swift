//
//  DSDrawableImageViewDelegate.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 19/08/2023.
//

import Foundation

/// A protocol that defines a delegate for a drawable view.
public protocol DSDrawableImageViewDelegate: AnyObject {

    /// Notifies the delegate that a touch interaction has begun on the drawable view.
    ///
    /// - Parameter point: The starting point of the touch interaction.
    func drawableImageViewBeginTouch(atStartPoint point: CGPoint)

    /// Notifies the delegate that drawing a new line has been completed.
    ///
    /// - Parameter point: The end point of the drawn line.
    func drawableImageViewDidFinishDrawNewLine(atEndPoint point: CGPoint)

    /// Notifies the delegate that drawing a new shape has been completed.
    ///
    /// - Parameter point: The end point of the drawn shape.
    func drawableImageViewDidFinishDrawNewShape(atEndPoint point: CGPoint)
}
