//
//  DSButtonSize.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 19/08/2023.
//

import Foundation

public enum DSButtonSize {

    /// Represents a small button size.
    case small

    /// Represents a medium button size.
    case medium

    /// Represents a large button size.
    case large

    /// Height value corresponding to the button size.
    var height: CGFloat {
        switch self {
        case .small:
            return .spacingSize10
        case .medium:
            return .spacingSize12
        case .large:
            return .spacingSize14
        }
    }
}
