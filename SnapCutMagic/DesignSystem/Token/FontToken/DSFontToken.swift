//
//  DSFontToken.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 21/08/2023.
//

import UIKit

extension UIFont {

    /// Font size: 12, Weight: Regular
    static let footnote = UIFont.systemFont(ofSize: 12)

    /// Font size: 12, Weight: Bold
    static let footnoteBold = UIFont.systemFont(ofSize: 12, weight: .bold)

    /// Font size: 16, Weight: Regular
    static let paragraph1 = UIFont.systemFont(ofSize: 16)

    /// Font size: 16, Weight: Bold
    static let paragraph1Bold = UIFont.systemFont(ofSize: 16, weight: .bold)

    /// Font size: 20, Weight: Regular
    static let paragraph2 = UIFont.systemFont(ofSize: 20)

    /// Font size: 20, Weight: Bold
    static let paragraph2Bold = UIFont.systemFont(ofSize: 20, weight: .bold)
}
