//
//  SCHomeRouter.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 19/08/2023.
//

import Foundation
import PanModal

public final class SCHomeRouter {

}

extension SCHomeRouter: SCHomeRouterProtocol {

    public func showPencilSettings(from parent: UIViewController, delegate: SCPencilSettingsDelegate?) {
        let viewModel = SCPencilSettingsViewModel()
        let controller = SCPencilSettingsViewController(viewModel: viewModel)
        controller.delegate = delegate
        parent.presentPanModal(controller)
    }

    public func showSavedImageAlert(from parent: UIViewController, error: Error?) {
        if let error = error {
            let alertController = UIAlertController(
                title: L10n.Home.SavingImage.Status.Error.title,
                message: error.localizedDescription,
                preferredStyle: .alert
            )
            alertController.addAction(
                UIAlertAction(
                    title: L10n.Common.Action.continue,
                style: .default
                )
            )
            parent.present(alertController, animated: true)
        } else {
            let alertController = UIAlertController(
                title: L10n.Home.SavingImage.Status.Success.title,
                message: L10n.Home.SavingImage.Status.Success.message,
                preferredStyle: .alert
            )
            alertController.addAction(
                UIAlertAction(
                    title: L10n.Common.Action.continue,
                    style: .default
                )
            )
            parent.present(alertController, animated: true)
        }
    }

}
