//
//  SCPencilSettingsViewController.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 19/08/2023.
//

import UIKit
import PanModal
import RxSwift
import RxCocoa

final class SCPencilSettingsViewController: UIViewController {

    // MARK: - UI

    private lazy var containerView = makeContainerView()

    private lazy var pencilColorContainerView = makePencilColorContainerView()
    private lazy var pencilColorPickerTitleLabel = makePencilColorPickerTitleLabel()
    private lazy var pencilColorPickerButton = makePencilColorPickerButton()

    // MARK: - Dependencies

    weak var delegate: SCPencilSettingsDelegate?
    private let viewModel: SCPencilSettingsViewModelProtocol
    private let disposeBag = DisposeBag()

    // MARK: - Initializers

    init(viewModel: SCPencilSettingsViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        defer {
            viewModel.fetch()
        }
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        setupBinding()
    }
}

// MARK: - PanModalPresentable

extension SCPencilSettingsViewController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return nil
    }

    var longFormHeight: PanModalHeight {
        return .intrinsicHeight
    }

    var shortFormHeight: PanModalHeight {
        return .intrinsicHeight
    }
}

// MARK: - UIColorPickerViewControllerDelegate

extension SCPencilSettingsViewController: UIColorPickerViewControllerDelegate {

    func colorPickerViewControllerDidFinish(_ viewController: UIColorPickerViewController) {}

    func colorPickerViewControllerDidSelectColor(_ viewController: UIColorPickerViewController) {
        viewModel.updatePencilColor(viewController.selectedColor)
    }
}

// MARK: - Privates

extension SCPencilSettingsViewController {

    private func setupViews() {
        setupConstraints()
    }

    private func setupBinding() {
        viewModel.pencilSettings
            .subscribe(onNext: { [weak self] settings in
                self?.updateUI(by: settings)
                self?.delegate?.pencilSettingsDidUpdate(settings)
            })
            .disposed(by: disposeBag)
    }

    private func updateUI(by settings: SCPencilSettings) {
        pencilColorPickerButton.backgroundColor = settings.pencilColor
    }

    private func setupConstraints() {
        var constraints: [NSLayoutConstraint] = []

        constraints += constraintsContainerView()
        constraints += constraintsPencilColorPickerButton()

        NSLayoutConstraint.activate(constraints)
    }

    private func constraintsContainerView() -> [NSLayoutConstraint] {
        view.addSubview(containerView)

        containerView.addArrangedSubview(pencilColorContainerView)

        pencilColorContainerView.addArrangedSubview(pencilColorPickerTitleLabel)
        pencilColorContainerView.addArrangedSubview(pencilColorPickerButton)

        return containerView.edgesAnchor.constraint(
            equalTo: view.edgesAnchor,
            insets: UIEdgeInsets(
                top: .spacingSize4,
                left: .spacingSize4,
                bottom: -.spacingSize13,
                right: -.spacingSize4
            )
        )
    }

    private func constraintsPencilColorPickerButton() -> [NSLayoutConstraint] {
        return [
            pencilColorPickerButton.heightAnchor.constraint(equalToConstant: .spacingSize10),
            pencilColorPickerButton.widthAnchor.constraint(equalTo: pencilColorPickerButton.heightAnchor)
        ]
    }

    private func makeContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .fill
        view.spacing = .spacingSize4
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makePencilColorContainerView() -> UIStackView {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .equalCentering
        view.alignment = .center
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    private func makePencilColorPickerTitleLabel() -> UILabel {
        let label = UILabel()
        label.font = .paragraph1Bold
        label.textColor = .black
        label.numberOfLines = 1
        label.text = L10n.PencilSettings.PickPencilColor.title
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }

    private func makePencilColorPickerButton() -> UIView {
        let view = UIView()

        let tapGesture = UITapGestureRecognizer()
        tapGesture.numberOfTapsRequired = 1
        tapGesture.addTarget(self, action: #selector(onPickPencilColor))

        view.addGestureRecognizer(tapGesture)
        view.backgroundColor = .red
        view.layer.cornerRadius = .spacingSize10 / 2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    @objc private func onPickPencilColor() {
        let picker = UIColorPickerViewController()
        picker.delegate = self
        picker.selectedColor = pencilColorPickerButton.backgroundColor ?? UIColor.red
        present(picker, animated: true)
    }
}
