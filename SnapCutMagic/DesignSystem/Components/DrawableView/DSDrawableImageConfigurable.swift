//
//  DSDrawableImageConfigurable.swift
//  SnapCutMagic
//
//  Created by Tuan Pham on 19/08/2023.
//

import UIKit

/// A protocol that defines methods for configuring the appearance of a drawable image.
public protocol DSDrawableImageConfigurable {

    /// Sets the width of the lines drawn on the drawable image.
    ///
    /// - Parameter width: The width of the lines.
    func setLineWidth(_ width: CGFloat)

    /// Sets the color of the shapes drawn on the drawable image.
    ///
    /// - Parameter color: The color of the shapes.
    func setShapeColor(_ color: UIColor)

    /// Sets the drawable image asset type.
    ///
    /// - Parameter asset: The type of asset, either local image or remote image URL.
    func setDrawableImage(_ asset: DSAssetType)
}
